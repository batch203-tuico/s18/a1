// console.log("Hello World");

function addTwoNumbers(num1, num2){
	console.log("Displayed sum of " + num1 + " and " + num2);
	console.log(num1 + num2);
}


function subtractTwoNumbers(num1, num2){
	console.log("Displayed difference of " + num1 + " and " + num2);
	console.log(num1 - num2);
}

addTwoNumbers(5, 15);
subtractTwoNumbers(20, 5);


function multiplyTwoNumbers(num1, num2){
	console.log("The product of " + num1 + " and " + num2 + ":");
	return num1 * num2;
	
}



function divideTwoNumbers(num1, num2){
	console.log("The quotient of " + num1 + " and " + num2 + ":");
	return num1 / num2;
}


let product = multiplyTwoNumbers(50, 10);
 console.log(product);
let quotient = divideTwoNumbers(50, 10);
console.log(quotient);



function computeAreaOfCircle(radius){
	console.log("The result of getting the area of a circle with " + radius + " radius:");
	return (Math.PI * (radius ** 2));
}

let circleArea = computeAreaOfCircle(15);
console.log(circleArea);



function averageOfFourNumbers(num1, num2, num3, num4){
	console.log("The average of " + num1 + "," + num2 + "," + num3 + "," + " and " + num4 + ":");
	return (num1 + num2 + num3 + num4) / 4;
}

let averageVar = averageOfFourNumbers(20, 40, 60, 80);
console.log(averageVar);



function checkPassingScore(yourScore, totalScore){
	console.log("Is " + yourScore + "/" + totalScore + " a passing score?")
	let percentage = ((yourScore / totalScore) * 100);
	let passingRate = 75
	return percentage >= passingRate;
}

let isPassingScore = checkPassingScore(38, 50);
console.log(isPassingScore);